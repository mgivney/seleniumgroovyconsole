package com.mattgivney.selenium.console

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.logging.LoggingPreferences
import org.openqa.selenium.logging.LogType

class FirefoxWebDriverSetup {
    
    private FirefoxWebDriverSetup singleton
    private WebDriver driver
    private String startUrl
    
    FirefoxWebDriverSetup() {
        DesiredCapabilities foxCap = DesiredCapabilities.firefox()
        FirefoxProfile profile = new FirefoxProfile()
        foxCap.setCapability(FirefoxDriver.PROFILE, profile)
        this.driver = new FirefoxDriver(foxCap)
    }

    public WebDriver getDriver(){
        return driver
    }
    
    void close(){
        driver.close()
    }
    
//FirefoxProfile profile = new FirefoxProfile();
//profile.setPreference( "browser.download.dir", "/home/lilian/downloads" );
//profile.setPreference( "browser.download.folderList", 2 );
//profile.setPreference( "browser.helperApps.neverAsk.saveToDisk", "application/pdf" );
//
//driver = new FirefoxDriver( profile );
//
//another option is to just download via Java
//URL url = new URL(urlToGet);
//URLConnection conn = url.openConnection();
//HttpURLConnection httpConn = (HttpURLConnection) conn;
//httpConn.setAllowUserInteraction(false);
//httpConn.setConnectTimeout(CONNECT_TIMEOUT);
//httpConn.setReadTimeout(SOCKET_TIMEOUT);
//httpConn.setInstanceFollowRedirects(true);
//httpConn.setRequestMethod("GET");
//httpConn.setRequestProperty("Cookie", myCookie);
//httpConn.connect();
//response = httpConn.getResponseCode();
//if (response == HttpURLConnection.HTTP_OK) {
//	in = httpConn.getInputStream();
//}
//FileOutputStream myFileOutputStream = new FileOutputStream("/tmp/foobar");
//byte buf[]=new byte[1024];
//int len;
//while((len=in.read(buf))&gt;0)
//	myFileOutputStream.write(buf,0,len);
//myFileOutputStream.close();
}