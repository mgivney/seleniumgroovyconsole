package com.mattgivney.selenium.console

import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.By as By
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class SeleniumEnabledGroovyConsole {

    static Logger logger = LoggerFactory.getLogger(SeleniumEnabledGroovyConsole.class)
    
    static void main(args){
        FirefoxWebDriverSetup setup = new FirefoxWebDriverSetup()
        Actions actions = new Actions(setup.getDriver())
        
        ConsoleRunner runner = new ConsoleRunner()
        runner.setVar("driver", setup.getDriver())
        runner.setVar("actions", actions)
        runner.setVar("logger", logger)
        //runner.setVar("startUrl", 'http://www.google.com')
        runner.setVar("By", By)
        runner.run()
        
        setup.close()
        System.exit(0)
    }
    
    
}