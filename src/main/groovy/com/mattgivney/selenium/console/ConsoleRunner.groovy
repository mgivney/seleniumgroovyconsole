package com.mattgivney.selenium.console;

import groovy.lang.Binding;
import groovy.ui.Console;

class ConsoleRunner {

    Console console
    Object sourceCode
    Boolean complete = false
    
//    ConsoleRunner(Console console){
//        this.console = console
//    }
    
    ConsoleRunner(Object sourceCode){
        this.console = new Console(
            this.class.classLoader,
            new Binding())
        this.sourceCode = sourceCode
        console.setVariable("sourceCode", sourceCode)
    }
    
    void setVar(String key, Object val){
        console?.setVariable(key,val)
    }
    
    void setVar(String key, List vals){
        console?.setVariable(key,vals)
    }
    
    void setVar(String key, Object[] vals){
        console?.setVariable(key,vals)
    }
    
    void run(){
        console.run()
        console.frame.windowClosing = this.&exit
        console.frame.windowClosed = this.&exit
        while(!complete){
            sleep 1000
        }
    }
    
    void exit(EventObject evt = null){
        complete = true
    }
    
    static void main(args){
        
    }
    
}