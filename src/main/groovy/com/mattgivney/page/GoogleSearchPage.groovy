package com.mattgivney.page

import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.By as By
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver

class GoogleSearchPage {

    WebElement searchText
    WebElement searchButton
    
    GoogleSearchPage(WebDriver driver){
        driver.get('http://www.google.com')
        searchText = driver.findElement(By.id('lst-ib'))
        searchButton = driver.findElement(By.name('btnG'))    
    }
    
    void setSearchText(String text){
        searchText.sendKeys(text)
    }
    
    void clickSearchButton(){
        searchButton.click()
    }
    
}