#  Selenium Powered Groovy Console #

Selenium is a very powerful test automation tool with a pretty large community.  Getting feedback while developing and (more specifically) debugging Selenium tests is pretty tedious.  This utility allows you to load your Selenium test project's classpath into a Groovy Console and execute your steps without recompiling, no testing framework required.

When the GroovyConsole starts, there are a few elements that are bound to variables in the console which you can use anywhere in your script:

variable | description
:-------:|:----------
driver   | The FireFox WebDriver
actions  | The org.openqa.selenium.interactions.Actions instance
logger   | an SLF4J logger
By       | The org.openqa.selenium.By class, so you don't need the import

### How to run it ###

Running the console is very simple. From your command line you type:

```
#!bash

./gradlew run
```

This command will download the required libraries, launch Firefox and launch a Groovy Console with Selenium embedded on the classpath. 

**Note: An install of the Java 8 SDK and the Firefox browser are required pre-requisites for this to run. All other artifacts will download**

### Page object example ###
In addition to the console runner, there is also a simple use case where I have created a Page object to represent the Google Search page.  This is a pretty mindless example, but I was trying to demo that you can add other classes to the console that you might want to use (i.e. utilities, abstractions etc.)

After running the SeleniumEnabledGroovyConsole, it will bring up the Groovy Console fully loaded.  You can run a demo of the GoogleSearchPage by executing:

```
#!groovy

import com.mattgivney.page.*

def searchPage = new GoogleSearchPage(driver)
searchPage.searchText = 'Republican Debate'
searchPage.clickSearchButton()
```


### To Do Items ###
* Currently, the console only supports Firefox.  I will add Chrome and IE to the list of supported drivers in the very near future
* Added args support.  I'd like to be able to send in a set of named args that get bound to the console so they can be referenced as: *props.variable*. I'd like this capability via command line or passing in a properties file.

### License ###
**The MIT License (MIT)**

Copyright (c) <2015> Matt Givney

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.